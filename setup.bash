#!/bin/bash
#
# Link dotfiles to home, if they don't exist.

set -euo pipefail

# Link dotfile to home if it does not currently exist.
link_file() {
  file="${1}"
  if [ ! -e ~/"${file}" ] && [ ! -L ~/"${file}" ]; then
      echo "${file} not found, linking." >&2
      ln -s "${PWD}"/"${file}" ~/"${file}"
  else
      echo "${file} found, ignoring." >&2
  fi
}

files=(.Xresources .bashrc .bash_profile .tmux.conf .inputrc .vimrc .Rprofile \
       .bcrc .editrc)

# Link each file to the home folder.
for dotfile in ${files[*]}; do
  link_file "${dotfile}"
done

# Extend the xterm-256color terminfo profile to include italics.
tic xterm-256color-italics.terminfo

# If current operating system is macOS, do the following.
if [ "$(uname)" == "Darwin" ]; then
  # Check whether Homebrew is installed or not.
  which -s brew
  if [[ $? != 0 ]] ; then
    # Install Homebrew
    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
  else
    brew update
  fi
  # Install GNU Coreutils.
  brew install \
    coreutils \
    findutils \
    gnu-tar \
    gnu-sed \
    gawk \
    gnutls \
    gnu-indent \
    gnu-getopt
fi
