# ~/.bashrc: executed for interactive Bash shells.

# If not running interactively, don't do anything.
case $- in
  *i*) ;;
  *) return;;
esac

NORMAL='\[\e[0m\]'
BLUE='\[\e[1;94m\]'
GREEN='\[\e[1;92m\]'

# If this is a non-root user, set the prompt to 'dir $ '. If this is a remote
# connection, set the prompt to 'user$host:dir $ '.
set_primary_prompt() {
  local host=''
  [[ $SSH_TTY ]] && host="${GREEN}\\u@\\H${NORMAL}:${BLUE}\\w${NORMAL} \$ "
  echo "${host}${BLUE}\\w${NORMAL} \$ "

}
PS1=$(set_primary_prompt)

PROMPT_DIRTRIM=2 # trim long paths in prompt

# Use bash-completion, if available.
# if [ -f /etc/bash_completion ]; then
#   ./etc/bash_completion
# fi

# Perform file completion in a case insensitive fashion.
bind 'set completion-ignore-case on'

# Treat hyphens and underscores as equivalent.
bind 'set completion-map-case on'

# Display matches for ambiguous patterns at first tab press.
bind 'set show-all-if-ambiguous on'

# Update window size after every command.
shopt -s checkwinsize

# Turn on recursive globbing (enables ** to recurse all directories).
shopt -s globstar 2> /dev/null

# Prepend cd to directory names automatically.
shopt -s autocd 2> /dev/null

# Correct spelling errors during tab-completion.
shopt -s dirspell 2> /dev/null

# Correct spelling errors in arguments supplied to cd.
shopt -s cdspell 2> /dev/null History

# Don't overwrite an existing file with the >, >&, and <> redirection
# operators. Override by using >| instead of >.
set -o noclobber

# Prevent the terminal from exiting when typing Ctrl-D.
export IGNOREEOF=2

# Set my preferred editor.
export VISUAL=vim
export EDITOR="$VISUAL"

# Only run the following if the current operating system is macOS.
if [ "$(uname)" == "Darwin" ]
then
  export CLICOLOR=1 # enable color output for ls
  export LSCOLORS='ExGxFxdaCxDaDaabadaeac' # set colorscheme for ls
fi

# Make less more friendly for non-text input files, see lesspipe(1).
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# Add an alias to enable syntax highlightning in less.
alias lessh='LESSOPEN="| /usr/bin/src-hilite-lesspipe.sh %s" less -M '

# less preferences, consult the less manual for further explanation.
export LESS='-iJMnQRS '

# Append to the history file, don't overwrite it.
shopt -s histappend

# Save multi-line commands as one command.
shopt -s cmdhist

# Avoid duplicate history entries.
HISTCONTROL='erasedups:ignoreboth'

# Record each line as it gets issued.
PROMPT_COMMAND='history -a'

# Never-ending history.
export HISTSIZE=
export HISTFILESIZE=

# Add an entry-number and timestamp to commands in history.
HISTTIMEFORMAT="%F %T: "

# Common aliases.
alias vi='vim'
alias ipython='ipython3'

# Enable color support of common tools.
if [[ "$(uname)" = Linux* ]]; then
  alias ls='ls --color=auto'
else
  alias ls='ls -G'
fi
alias dir='dir --color=auto'
alias vdir='vdir --color=auto'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'

# Most servers won't recognize xterm-256color-italic.
alias ssh='TERM=xterm-256color ssh'

# Always confirm file removal.
alias rm='rm -i'

# Load bc settings.
export BC_ENV_ARGS=~/.bcrc

# Disable bc's startup message and load the math library.
alias bc='bc --mathlib --quiet'
alias fastqc='/home/feli/Applications/FastQC/fastqc'

export PATH="$HOME/anaconda/bin:$PATH"
export PATH="/usr/local/bin:/usr/local/sbin:$PATH"
export PATH="/Users/feli/Documents/Projekt/infer_orthologs/scripts/src_and_wrapper_scripts:$PATH"
export PATH="/usr/local/lib/ruby/gems/2.5.0/bin:$PATH"

# # added by Anaconda2 2018.12 installer
# # >>> conda init >>>
# # !! Contents within this block are managed by 'conda init' !!
# __conda_setup="$(CONDA_REPORT_ERRORS=false '/home/feli/anaconda2/bin/conda' shell.bash hook 2> /dev/null)"
# if [ $? -eq 0 ]; then
#     \eval "$__conda_setup"
# else
#     if [ -f "/home/feli/anaconda2/etc/profile.d/conda.sh" ]; then
#         . "/home/feli/anaconda2/etc/profile.d/conda.sh"
#         CONDA_CHANGEPS1=false conda activate base
#     else
#         \export PATH="/home/feli/anaconda2/bin:$PATH"
#     fi
# fi
# unset __conda_setup
# # <<< conda init <<<
# # added by Anaconda3 2018.12 installer
# # >>> conda init >>>
# # !! Contents within this block are managed by 'conda init' !!
# __conda_setup="$(CONDA_REPORT_ERRORS=false '/home/feli/anaconda3/bin/conda' shell.bash hook 2> /dev/null)"
# if [ $? -eq 0 ]; then
#     \eval "$__conda_setup"
# else
#     if [ -f "/home/feli/anaconda3/etc/profile.d/conda.sh" ]; then
#         . "/home/feli/anaconda3/etc/profile.d/conda.sh"
#         CONDA_CHANGEPS1=false conda activate base
#     else
#         \export PATH="/home/feli/anaconda3/bin:$PATH"
#     fi
# fi
# unset __conda_setup
# # <<< conda init <<<
