#!/bin/bash
#
# Remove dotfiles from the home folder, use carefully!

set -euo pipefail

# Remove dotfile if it exist.
rm_file() {
  file="${1}"
  if [ ! -e ~/"${file}" ] && [ ! -L ~/"${file}" ]; then
      echo "${file} not found, ignoring." >&2
  else
      echo "${file} found, removing." >&2
      rm ~/"${file}"
  fi
}

files=(.Xresources .bashrc .bash_profile .tmux.conf .inputrc .vimrc .Rprofile \
       .bcrc)

# Link each file to the home folder.
for dotfile in ${files[*]}; do
  rm_file "${dotfile}"
done
